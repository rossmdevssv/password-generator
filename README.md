Secure Password Generator | Web-Based Version!
======

#### Last Updated: ``Feb 6th, 2019``
#### Configured: Moved code from GitHub to BitBucket & Small Grammatical Fixes

# What PasswordGen uses
* ExpressJS : Fast, unopinionated, minimalist web framework for Node.js
* Node.js: an open-source, cross-platform JavaScript run-time environment
* OPN: Client Side File/App Execution
* HTML: Front-End Design Code
* Ports: Magic :) we know!
* A little bit of help from some people!
* JS: interpreted programming language
* Console Logging: Sends messages to the console to identify information (Ports, Build Info etc.)

# What you'll need
 
* Launcher.bat (Provided in bundle)

# How to install

* Download [Master.zip] (Download from this repo or email support@rossmdevssv.atlassian.net)
* Extract the files to your desktop
* Open `launcher.bat` and start from 1 and proceed to the bottom! 

# PLEASE INSTALL THESE 

* [NODEJS](https://nodejs.org/en/)
*  Not Required for recruits >> [GitBash](https://git-scm.com/downloads)
** Shall you encounter errors; contact our [Support Desk](https://rossmdevssv.atlassian.net/servicedesk/customer/portal/)!

##### The console should say: 

Secure Password Generator 1.0 | Created by Ross & Intel
<br/>
Opening in browser now
<br/>
Press CONTROL + C to terminate session!
<br/>
Listening on http://localhost:3000

